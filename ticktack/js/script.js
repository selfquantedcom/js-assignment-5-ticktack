class Board{
	
	constructor(){
		this.dim1 = 3;
		this.dim2 = 3; 

		//keep track of who made the last move
		this.lastPlayer = 2;
		
		//Create dim1 x dim2 grid with 0
		let grid = new Array(this.dim1);
		for (let i = 0; i < this.dim1; i++){						
			grid[i] = new Array(this.dim2);

			for (let j = 0; j < this.dim2; j++){
				grid[i][j] = 0;
			}
		}			
		this.grid = grid;
	}

	move(cordX, cordY, player){		
		this.grid[cordX][cordY] = player;	
		this.lastPlayer = player;
	}
}

//global variables
let myBoard = new Board();

//start new game
function newGame(){
	for (let i = 1; i < 4; i++){
		for (let j = 1; j < 4; j++){
			displaySymbol(1, i, j, "hidden");
			displaySymbol(2, i, j, "hidden");			
		}
	}
	end = false;	
	myBoard = new Board();
}

//display square/cross at a specific location
function displaySymbol(player,cordX, cordY, visibility){
	let className = "player" + String(player) + "-" + cordX + "-" + cordY;
	document.getElementsByClassName(className)[0].style.visibility = visibility;

	//dhange player 
	if (player === 1){
		myBoard.lastPlayer = 2;
	} else{
		myBoard.lastPlayer = 1;
	}
	myBoard.grid[cordX-1][cordY-1] = myBoard.lastPlayer;	
	
}

//get X,Y coordinates from a class name
function getCoordinates(className){	
	let temp = className.split("-");
	let coordinates = [temp[1], temp[2]];	
	return coordinates
}

function getCurrentPlayer(){
	return myBoard.lastPlayer
}
function isEmpty(cordX, cordY){
	if(myBoard.grid[cordX-1][cordY-1] === 0){
		return true
	} else{
		return false
	}
}

function makeMove(className){
	if(!checkWin()){
		let cordX = getCoordinates(className)[0];
		let cordY = getCoordinates(className)[1];
		let player = getCurrentPlayer();

		if(isEmpty(cordX, cordY)){
			displaySymbol(player, cordX, cordY, "visible");
		}	
	}
}

function checkWin(){
	
	let horizontalArr = [];	
	//horizontaly
	for (let i = 0; i < 3; i++){
		for (let j = 0; j < 3; j++){
			horizontalArr.push(myBoard.grid[i][j]);
		}
		if (horizontalArr[0] === horizontalArr[1] && horizontalArr[1] === horizontalArr[2] && sumArray(horizontalArr) !== 0){
			if(horizontalArr[0] === 1){
				console.log("Player1 won!");					
				return true
			} else{
				console.log("Player2 won!");					
				return true
			}
			
		}
		horizontalArr = [];		
	}

	let verticalArr = [];	
	//vertically
	for (let j = 0; j < 3; j++){
		for (let i = 0; i < 3; i++){
			verticalArr.push(myBoard.grid[i][j]);			
		}
		if (verticalArr[0] === verticalArr[1] && verticalArr[1] === verticalArr[2] && sumArray(verticalArr) !== 0){
			if(verticalArr[0] === 1){
				console.log("Player1 won!");					
				return true
			} else{
				console.log("Player2 won!");					
				return true
			}
			
		}
		verticalArr = [];		
	}

	//diagonaly
	let diagonal1Arr = [];	
	diagonal1Arr.push(myBoard.grid[0][0]);			
	diagonal1Arr.push(myBoard.grid[1][1]);			
	diagonal1Arr.push(myBoard.grid[2][2]);			
	
	let diagonal2Arr = [];
	diagonal2Arr.push(myBoard.grid[0][2]);			
	diagonal2Arr.push(myBoard.grid[1][1]);			
	diagonal2Arr.push(myBoard.grid[2][0]);

	if (diagonal1Arr[0] === diagonal1Arr[1] && diagonal1Arr[1] === diagonal1Arr[2] && sumArray(diagonal1Arr) !== 0){
		if(diagonal1Arr[0] === 1){
			console.log("Player1 won!");				
			return true
		} else{
			console.log("Player2 won!");				
			return true
		}
	}
	diagonal1Arr = [];		

	if (diagonal2Arr[0] === diagonal2Arr[1] && diagonal2Arr[1] === diagonal2Arr[2] && sumArray(diagonal2Arr) !== 0){
		if(diagonal2Arr[0] === 1){
			console.log("Player1 won!");	
			end = true;
		} else{
			console.log("Player2 won!");
			end = true;	
		}
	}
	diagonal2Arr = [];		
}

function sumArray(array){
	let sum = 0;
	for (let i = 0; i < array.length; i++){
		sum += array[i];
	}
	return sum
}












