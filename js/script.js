//Create new board and distinguish the human/machine
let myBoard = [[0,0,0], [0,0,0], [0,0,0]];
let lastPlayer = 2;
let playAI = false; 
let scorePlayer1 = 0;
let scorePlayer2 = 0;

const humanPlayer = 1;
const machinePlayer = 2;
const simTrials = 100;

//start new game
function newGame(ai){
	for (let i = 0; i < 3; i++){
		for (let j = 0; j < 3; j++){
			displaySymbol(1, i, j, "hidden");
			displaySymbol(2, i, j, "hidden");	
		}
	}

	myBoard = [[0,0,0], [0,0,0], [0,0,0]];
	if (ai === true){
		lastPlayer = 1;		
		playAI = true;

		//AI makes first move (this increases tie rate dramatically!)
		let best = bestMove(myBoard);			
		lastPlayer = switchPlayer(lastPlayer);
		displaySymbol(lastPlayer,best[0], best[1], "visible");	

	} else if (ai === false){
		lastPlayer = 2;
	}
}	

function resetScore(){
	scorePlayer1 = 0;
	scorePlayer2 = 0;	
	document.getElementById("player1-score").innerHTML = "Player1 score: " + String(scorePlayer1);
	document.getElementById("player2-score").innerHTML = "Player2 score: " + String(scorePlayer2);
}


//display/hide a square/cross at a specific location
function displaySymbol(player,cordX, cordY, visibility){
	let className = "player" + String(player) + "-" + cordX + "-" + cordY;	
	document.getElementsByClassName(className)[0].style.visibility = visibility;	
	myBoard[cordX][cordY] = lastPlayer;		
}

//get X,Y coordinates from a class name
function getCoordinates(className){	
	let temp = className.split("-");
	let coordinates = [temp[1], temp[2]];	
	return coordinates
}

function switchPlayer(player){	
	if (player === 1){		
		return 2
	} else{			
		return 1
	}
}

function isEmpty(cordX, cordY){
	if(myBoard[cordX][cordY] === 0){
		return true
	} else{
		return false
	}
}

function updateScore(winningPlayer){
	//update score in browser
	if(winningPlayer === 2){
		scorePlayer2 += 1;
	} else if (winningPlayer === 1){
		scorePlayer1 += 1;
	}

	document.getElementById("player1-score").innerHTML = "Player1 score: " + String(scorePlayer1);
	document.getElementById("player2-score").innerHTML = "Player2 score: " + String(scorePlayer2);
	
}

function makeMove(className){

	//Only allow move if the game has not finished	
	if(!checkWin(myBoard)[0]){		
		let cordX = getCoordinates(className)[0];
		let cordY = getCoordinates(className)[1];
		lastPlayer = switchPlayer(lastPlayer);		
		
		//Make move if the field is empty
		if(isEmpty(cordX, cordY)){
			displaySymbol(lastPlayer, cordX, cordY, "visible");			
		}	

		//check for winner
		let winner = checkWin(myBoard);
		if(winner[0]){
			updateScore(winner[1])
			setTimeout(function(){ alert("Player " + winner[1] + " won!"); }, 50);
			return
		}

		//check for draw
		if (boardFull(myBoard) === true){
			setTimeout(function(){ alert("It's a draw!"); }, 50);
			return
		}

		//play AI
		if(playAI === true){
			//Machine player move		
			let best = bestMove(myBoard);			
			lastPlayer = switchPlayer(lastPlayer);
			displaySymbol(lastPlayer,best[0], best[1], "visible");	

			//check for winner
			winner = checkWin(myBoard);
			if(winner[0]){
				updateScore(winner[1])
				setTimeout(function(){ alert("Player " + winner[1] + " won!"); }, 50);
				return
			}

			//check for draw
			if (boardFull(myBoard) === true){
				setTimeout(function(){ alert("It's a draw!"); }, 50);
			}

		}

	}
}

//check whether either player has won and if so, which one
function checkWin(board){

	let winner;
	//Check for victory in a gorizontal row
	let array = [];	
	for (let i = 0; i < 3; i++){
		for (let j = 0; j < 3; j++){
			array.push(board[i][j]);
		}		
		winner = winningRow(array);
		array = [];
		if(winner[0]){
			return winner
		}
	}

	//Check for victory in a vertical column
	array = []

	for (let j = 0; j < 3; j++){
		for (let i = 0; i < 3; i++){
			array.push(board[i][j]);			
		}		
		winner = winningRow(array);
		array = []
		if(winner[0]){
			return winner
		}				
	}

	//Check for victory diagonaly
	let diagonal1Arr = [];	
	diagonal1Arr.push(board[0][0]);			
	diagonal1Arr.push(board[1][1]);			
	diagonal1Arr.push(board[2][2]);			

	let diagonal2Arr = [];
	diagonal2Arr.push(board[0][2]);			
	diagonal2Arr.push(board[1][1]);			
	diagonal2Arr.push(board[2][0]);

	winner = winningRow(diagonal1Arr);
	if(winner[0]){
		return winner
	}		

	winner = winningRow(diagonal2Arr);
	if(winner[0]){
		return winner
	}		
	return [false, null]
}

//Determine whether the passed in row is a winner and if so, which player won
function winningRow(array){

	if (array[0] === array[1] && array[1] === array[2] && sumArray(array) !== 0){
		if(array[0] === 1){			
			return [true, 1]

		} else if (array[0] === 2) {						
			return [true, 2]			
		}
	} else{
		return [false, null]
	}	
}

//determines whether the board is full
function boardFull(board){
	let status = true;
	for (let i = 0; i < 3; i++){
		for (let j = 0; j < 3; j++){
			if (board[i][j] === 0){
				status = false;
			}
		}
	}
	return status
}

//Sum elements in an array
function sumArray(array){
	let sum = 0;
	for (let i = 0; i < array.length; i++){
		sum += array[i];
	}
	return sum
}

//Tick Tack AI!
function bestMove(board){
	let machinePlayerWon;
	let localWinner;
	let localLastPlayer = lastPlayer;	

	//coppy original board: prevent obj duplication!
	let scoreBoard = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];

	for (let i = 0; i < 3; i++){
		for (let j = 0; j < 3; j++){			
			scoreBoard[i][j] = board[i][j];						
		}
	}	

	//indicate occupied fields to prevent move duplication
	for (let i = 0; i < 3; i++){
		for (let j = 0; j < 3; j++){
			if (scoreBoard[i][j] !== 0){
				scoreBoard[i][j] = - 1000;								 				
			}			
		}
	}
	
	//run simulation
	for (let temp = 0; temp < simTrials; temp++){		

		//create a local coppy of the game board
		let freshBoard = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];				

		for (let i = 0; i < 3; i++){
			for (let j = 0; j < 3; j++){			
				freshBoard[i][j] = board[i][j];						
			}
		}	
		
		//play random moves starting with machine player. Finish when game is over		
		let victory = false;
		localLastPlayer = switchPlayer(localLastPlayer);	

		while(!boardFull(freshBoard) && !victory){					
			for (let i = 0; i < 3; i++){				
				for (let j = 0; j < 3; j++){						
					
					if (freshBoard[i][j] === 0){
						
						if(!victory){
							let num = Math.floor((Math.random() * 2) + 1);	
							if (num === localLastPlayer){
								
								freshBoard[i][j] = localLastPlayer;								
								
								if(checkWin(freshBoard)[0]){
									victory = true;
									break 
								}							
								localLastPlayer = switchPlayer(localLastPlayer); 					
							}					
						}												
						
					}	
				}
			}
		}		

		//Determine winner				
		if(checkWin(freshBoard)[1] === humanPlayer){
			machinePlayerWon = false;
		}else if (checkWin(freshBoard)[1] === machinePlayer) {
			machinePlayerWon = true;
		} else{
			machinePlayerWon = null;
		}				

		//Score board: penalize loosing fields, reward winning fields
		for (let i = 0; i < 3; i++){
			for (let j = 0; j < 3; j++){		
				//skip draw
				if(machinePlayerWon !== null){
					//score board when human player won
					if(machinePlayerWon){	
						if(freshBoard[i][j] === machinePlayer){
							scoreBoard[i][j] += +1;				 					
						} else{
							scoreBoard[i][j] += -1;				 					
						}	
						
						//score board when machine player won					
					} else{
						if(freshBoard[i][j] === machinePlayer){
							scoreBoard[i][j] += -1;				 					
						} else{
							scoreBoard[i][j] += +1;				 					
						}
					}		
				}
			}
		}				
	}
	
	//pick the field with the highest score
	let highestScore = 0;
	let bestField = [0,0];

	for (let i = 0; i < 3; i++){
		for (let j = 0; j < 3; j++){
			if (scoreBoard[i][j] >= highestScore){
				highestScore = scoreBoard[i][j];
				bestField[0] = i;				 				
				bestField[1] = j;				 				
			}
		}
	}	
	return bestField
}












