"""
Monte Carlo Tic-Tac-Toe Player
"""

import random
import poc_ttt_gui
import poc_ttt_provided as provided

# Constants for Monte Carlo simulator
# You may change the values of these constants as desired, but
#  do not change their names.
NTRIALS = 50    	# Number of trials to run
SCORE_CURRENT = 1.0 # Score for squares played by the current player
SCORE_OTHER = 1.0   # Score for squares played by the other player



# Add your functions here.
def mc_trial(board, player):
    """make a move at a randomly selected empty position. Alternate
    between players. Return when game won/board full"""
    
    while(True):
        row = random.randrange(0,board.get_dim())
        col = random.randrange(0,board.get_dim())
        position = (row, col)
        
        if(position in board.get_empty_squares()):
            board.move(row, col, player)
            player = provided.switch_player(player)
            
            #has the game finished?
            if(board.check_win() is not None):
                return                    

                def mc_update_scores(scores, board, player):
                    """Atach score to individual points on the board"""
                    winner = board.check_win()
    #check for a draw
    if(winner == 4):
        return 
        
    #score when player O won
elif(winner == 3):
    for row in range(0,int(board.get_dim())):
        for col in range(0,int(board.get_dim())):
            if (player == 3):
                if(board.square(row,col) == 3):
                    scores[row][col] += SCORE_CURRENT 
                elif(board.square(row,col) == 2):
                    scores[row][col] -= SCORE_OTHER 
                else:
                    if(board.square(row,col) == 2):
                        scores[row][col] -= SCORE_CURRENT 
                    elif(board.square(row,col) == 3):
                        scores[row][col] += SCORE_OTHER 
                        
    #score when player X won
else:
    for row in range(0,int(board.get_dim())):
        for col in range(0,int(board.get_dim())):
            if (player == 2):
                if(board.square(row,col) == 2):
                    scores[row][col] += SCORE_CURRENT 
                elif(board.square(row,col) == 3):
                    scores[row][col] -= SCORE_OTHER  
                else:
                    if(board.square(row,col) == 3):
                        scores[row][col] -= SCORE_CURRENT 
                    elif(board.square(row,col) == 2):
                        scores[row][col] += SCORE_OTHER                       
                        
                        
    #print scores
    
    def get_best_move(board, scores):
        """Determine the best move"""
        
    #check whether the board is full
    if(len(board.get_empty_squares()) == 0):
        return
        
    #find scores of empty squares
    best_position = []
    highest_score = []
    for row in range(0, board.get_dim()):
        for col in range(0, board.get_dim()):
            if(board.square(row,col) == 1):
                best_position.append((row,col))               	
                highest_score.append(scores[row][col])
                
    #get position with the higest score    
    pos = highest_score.index(max(highest_score))
    return (best_position[pos]) 
    

    def mc_move(board, player, NTRIALS):
    """Uses a monte carlo simulation to determine 
    an optimal move """
    scores = []
    #initiate scores
    for dummy in range(0,board.get_dim()):
        scores.append(board.get_dim()*[0])
        
        for dummy in range(0,NTRIALS):
            board_clone = board.clone()
            mc_trial(board_clone, player)
            mc_update_scores(scores, board_clone, player)
            
            result = get_best_move(board,scores)
    #reset score
    del scores[:]
    
    return result
    
# Test game with the console or the GUI.  Uncomment whichever 
# you prefer.  Both should be commented out when you submit 
# for testing to save time.


#provided.play_game(mc_move, NTRIALS, False)        
#poc_ttt_gui.run_gui(3, provided.PLAYERX, mc_move, NTRIALS, False)
#mc_update_scores([[0, 0, 0], [0, 0, 0], [0, 0, 0]], provided.TTTBoard(3, False, [[provided.PLAYERX, provided.PLAYERX, provided.PLAYERO], [provided.PLAYERO, provided.PLAYERX, provided.EMPTY], [provided.EMPTY, provided.PLAYERX, provided.PLAYERO]]), 3) 
